#!/bin/bash

die(){
	echo "${1}"
	rm -r "${tmpdir}"
	exit 1
}

worker(){
	local artist="${1}"
	local album="${2}"
	local _eartist="$(echo -n "${artist}" | sed "s/'/%27/g;s/\"/%22/g;s/\+/%2B/g;s/\,/%2C/g;s/\:/%3A/g;s/\;/%3B/g;s/\=/%3D/g;s/\@/%40/g;s/\//%2F/g;s/\ /\+/g")"
	local _ealbum="$(echo -n "${album}" | sed "s/'/%27/g;s/\"/%22/g;s/\+/%2B/g;s/\,/%2C/g;s/\:/%3A/g;s/\;/%3B/g;s/\=/%3D/g;s/\@/%40/g;s/\//%2F/g;s/\ /\+/g")"
	local string="${_eartist}/${_ealbum}"

	local tmpdir="/tmp/octavia"
	# exit if another instance in work
	[ -d "${tmpdir}" ] && [ -e "${tmpdir}/html" ] && exit 0
	# exit if cover exist in cache
	local cache="${XDG_CACHE_HOME:-${HOME}/.cache}/octavia"
	[ -r "${cache}/${artist}/${album}.png" ] && exit 0
	# fetch html
	mkdir -p "${tmpdir}" || die
	wget -q --user-agent="Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/538.1 (KHTML, like Gecko) Gecko/20130401 Firefox/44.1" "http://www.last.fm/music/${string}" -O "${tmpdir}/html" || die "Can't fetch html page"
	# get albumart url
	local img_url=""
	img_url1="$(grep "akamaized.net" "${tmpdir}/html" | sed 's/"$//g;s/.*"//g')"
	img_url2="$(grep "lst.fm" "${tmpdir}/html" | sed 's/"$//g;s/.*"//g')"
	img_url="$(echo -e "${img_url1}\n${img_url2}" | grep "^http.*" | sed "s/.*jpe*g$//g;s/^$//g")"
	img_url="$(
		for l in ${img_url}; do
			echo "${l}"
		done
	)"
	img_url="$(echo "${img_url}" | tail -n 1)"
	[[ "${img_url}" == "" ]] && die "No link to image"
	wget -q "${img_url}" -O "${tmpdir}/img" || die "Can't download image"
	# convert albumart
	mkdir -p "${cache}/${artist}" || die "Can't create directory in cache"
	convert "${tmpdir}/img" -quiet -quality 9 -adaptive-resize 250x250 "${cache}/${artist}/${album}.png" || die "Can't convert or save image"
	# clear temporary files
	rm -r "${tmpdir}" || die "Can't remove temporary files"
	# update octavia status
}

# artist, album, artist/album(with convertion)
worker "${1}" "${2}"
exit 0
