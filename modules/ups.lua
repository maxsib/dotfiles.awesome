local parse = require("modules.parse")
-- local async_spawn = require("awful.spawn").easy_async
local theme = require("beautiful").get()
local timer = require("gears.timer").start_new
local wibox = require("wibox")
local naughty = require("naughty")

local ups = {
	mt = {}
}
local ups_data = {}
local ups_status = ""
local widgets = {}

local function split_status(str)
	local candidates = { "OL", "OB", "LB", "HB", "RB", "CHRG", "DISCHRG", "OFF" }
	local matches = parse.only_needed(str, candidates)
	return matches
end

local function parser(data)
	local replaces = {
		["battery.charge"] = "charge", -- current charging percentage
		["battery.charge.low"] = "low", -- critical charging value when UPS do shutdown
		["battery.charge.warning"] = "warning", -- minimum charging value when we should paint progressbar red
		-- ["device.mfr"] = "manufacturer", -- UPS (device) manufacturer
		-- ["device.model"] = "model", -- UPS (device) model
		["input.voltage"] = "in_voltage", -- input voltage (from AC to UPS)
		["output.voltage"] = "out_voltage", -- output voltage (from UPS to PC)
		["ups.load"] = "load", -- load (percentage)
		["ups.status"] = "status" -- status (online, battery, charging etc)
	}
	local tbl = parse.keyval(data, "([%w%.]*): (.*)", replaces)
	for _, i in pairs(replaces) do
		if tbl[i] == nil then
			if i == "manufacturer" then tbl[i] = "Unknown"
			elseif i == "model" then tbl[i] = "Unknown"
			elseif i == "status" then tbl[i] = "OFF"
			else tbl[i] = 0
			end
		end
	end
	tbl.in_voltage = math.floor(tbl.in_voltage)
	tbl.out_voltage = math.floor(tbl.out_voltage)
	tbl.status = split_status(tbl.status)
	return tbl
end

local function get_data(name)
	if name == nil then return nil end
	local f = io.popen("/usr/bin/upsc " .. name .. " 2>/dev/null")
	local data = parser(f:read("*all"))
	f:close()
	--[[ -- This doesn't work. It return a PID, not a stdout. o_O
	local data = {}
	async_spawn("/usr/bin/upsc " .. name, function(str)
		data = str
		return data
	end)
	--]]
	ups_data = data
	return data
end

local function status()
	local d = {}
	for _, i in pairs(ups_data["status"]) do
		if i == "OFF" or i == nil then
			d.b = "o"
			break
		elseif i == "RB" then
			d.b = "r"
			break
		elseif i == "OL" then
			d.b = "c"
		else
			for _, b in pairs({ "OB", "LB", "HB" }) do
				if i == b then
					d.b = "b"
					break
				end
			end
		end
		if i == "DISCHRG" then
			d.c = "d"
		elseif i == "CHRG" then
			d.c = "c"
		end
	end
	local st = ""
	local back, front, color
	if d.b == "o" or d.b == nil then
		st = st .. ", offline"
		back = theme.ups.nobat
		front = theme.ups.err
	else
		back = theme.ups.bat
		if d.b == "c" then
			st = st .. "mains supply"
			front = theme.ups.chrg
			if d.c == "c" then
				st = st .. " (charging)"
				color = "#00FF00"
			else
				color = "#FFFFFF"
			end
		else
			st = st .. "battery powered"
			front = theme.ups.none
			color = "#FFFFFF"
			if tonumber(ups_data["charge"]) <= tonumber(ups_data["warning"]) then
				st = st .. " (low)"
				color = "#FF4000"
			end
			if tonumber(ups_data["charge"]) <= tonumber(ups_data["low"]) then
				naughty.notify({
					icon = theme.icon.notify.bat, timeout = 5, title = "WARNING!",
					text = "Battery discharged! <span color='#FF4000'>" .. ups_data["charge"] .. "%</span> left."
				})
			end
		end
	end
	ups_status = st
	widgets.bar:set_color(color)
	widgets.background:set_image(back)
	widgets.foreground:set_image(front)
	widgets.bar:set_value(ups_data["charge"])
end

local function worker(name)
	local t = timer(5, function()
		ups_data = get_data(name)
		status()
		return true
	end)
	t:again()
	t:emit_signal("timeout")
end

function ups.init(name)
	widgets.background = wibox.widget.imagebox()
	widgets.foreground = wibox.widget.imagebox()
	widgets.bar = wibox.widget {
		widget = wibox.widget.progressbar,
		max_value = 100,
		value = 0,
		background_color = "#000000",
		color = "#FFFFFF"
	}
	local widget = wibox.widget {
		widgets.background,
		{
			wibox.container.margin(widgets.bar,7,7,10,10),
			forced_height = 4,
			forced_width = 10,
			direction = "east",
			layout = wibox.container.rotate
		},
		widgets.foreground,
		layout = wibox.layout.stack
	}
	local _n = nil
	widget:connect_signal("mouse::enter", function()
		if not _n then
			_n = naughty.notify({ icon = theme.icon.notify.bat, title = name, timeout = 5,
				text = "Charge: " .. ups_data["charge"] .. "%\n"
				.. "Status: " .. ups_status .. "\n"
				.. "Voltage (I/O): " .. ups_data["in_voltage"] .. "V/" .. ups_data["out_voltage"] .. "V\n"
				.. "Load: " .. ups_data["load"] .. "%"
			})
		end
	end)
	widget:connect_signal("mouse::leave", function()
		if _n then
			naughty.destroy(_n)
			_n = nil
		end
	end)
	worker(name)
	return widget
end

return ups
