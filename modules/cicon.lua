local theme = require("beautiful").get()

local function cicon(c)
	local _icon
	if c.class == "Mutt" then
		_icon = theme.icon.prg.mail
	elseif c.class == "Neovim" then
		_icon = theme.icon.prg.nvim
	elseif c.class == "Pinentry-gtk-2" then
		_icon = theme.icon.prg.gpg
	elseif c.class == "Gimp" then
		_icon = theme.icon.prg.gimp
	elseif c.class == "Xephyr" then
		_icon = theme.icon.prg.x
	elseif c.class == "Ranger" then
		_icon = theme.icon.prg.fm
	elseif c.class == "Player" then
		_icon = theme.icon.prg.music
	elseif c.class == "Alsamixer" then
		_icon = theme.icon.prg.alsa
	elseif c.class == "Htop" then
		_icon = theme.icon.prg.sysmon
	elseif c.class == "qutebrowser" then
		_icon = theme.icon.prg.web
	elseif c.class == "Poezio" then
		_icon = theme.icon.prg.jabber
	elseif c.class == "st-256color" then
		_icon = theme.icon.prg.term
	else
		if c.name == "Event Tester" then
			_icon = theme.icon.prg.kbd
		else
			_icon = c.icon and c.icon or theme.icon.prg.no_icon
		end
	end
	return _icon
end

return cicon
