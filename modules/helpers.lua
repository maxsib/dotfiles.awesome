local theme = require("beautiful").get()
local notify = require("naughty").notify
local shell = require("awful.spawn").with_shell

local icon_theme = require("menubar.icon_theme")

local helpers = {}

function helpers.icon(name, size)
	if not name then return end
	size = size or 24
	local f = icon_theme(theme.icon_theme, { "/usr/share/icons" }):find_icon_path(name, size)
	return f or icon_theme(theme.icon_theme, { "/usr/share/icons" }):find_icon_path("dialog-question", size)
end

function helpers.client_info(c)
	if not c then c = client.focus end
	local str = ""
	if c ~= nil then
		if c.pid then str = str .. "Pid: «" .. tostring(c.pid) .. "»" end
		if c.type then str = str .. "\nType: «" .. tostring(c.type) .. "»" end
		if c.class then str = str .. "\nClass: «" .. tostring(c.class) .. "»" end
		if c.instance then str = str .. "\nInstance: «" .. tostring(c.instance) .. "»" end
		if c.role then str = str .. "\nRole: «" .. tostring(c.role) .. "»" end
		if c.name then str = str .. "\nName: «" .. tostring(c.name) .. "»" end
		if c.icon then str = str .. "\nIcon: «" .. tostring(c.icon) .. "»" end
	end
	if str ~= "" then
		notify({
			icon = c.icon or theme.icon.notify.i,
			timeout = 0,
			title = "Client information:",
			text = str
		})
	end
end

function helpers.killer(c)
	if not c then c = client.focus end
	if c then
		local killed = {
			pid = c.pid,
			class = c.class,
			icon = c.icon
		}
		shell("kill -KILL " .. killed.pid)
		notify({
			icon = killed.icon,
			title = "Killer report:",
			text = "PID <b>" .. killed.pid .. "</b> (<i>" .. killed.class .. "</i>) was killed"
		})
	end
end

return helpers
