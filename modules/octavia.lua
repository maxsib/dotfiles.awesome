local parse = require("modules.parse")
local timer = require("gears.timer").start_new
local wibox = require("wibox")
local notify = require("naughty").notify
local table_join = require("awful.util").table.join
local button = require("awful.button")
local theme = require("beautiful").get()
local util = require("awful.util")
local shape = require("gears.shape")
local match = require("awful.rules").match
local run_or_raise = require("awful.client").run_or_raise
local spawn = require("awful.spawn").with_shell

local cachedir = os.getenv("XDG_CACHE_HOME") .. "/octavia/"
local configdir = util.get_configuration_dir()

local octavia = {
	data = {}
}

local _defaults = {
	timeout = 5,
	host = "127.0.0.1",
	port = "6600",
	player = "/usr/bin/st -c 'Player' -e /usr/bin/ncmpcpp -s playlist -S visualizer"
}

local _initialized = false
local _volume = {
	muted = false,
	saved_volume = 0,
	dontupdate = false
}
local _seek_dontupdate = false
local _nodata = { ["volume"] = 0, ["repeat"] = "0", ["random"] = "0",
	["single"] = "0", ["consume"] = "0", ["playlist_length"] = "0",
	["state"] = "disconnected", ["song"] = "0", ["elapsed"] = 0, ["bitrate"] = 0,
	["next_song"] = 0, ["file"] = "NONE", ["date"] = "0000",
	["album"] = "Unknown", ["title"] = "DISCONNECTED", ["artist"] = "Unknown",
	["genre"] = "Unknown", ["duration"] = 0
}
local w = {
	d = {},
	p = {}
}

local function parser(str)
	if not str then return nil end
	local replaces = {
		["volume"] = "volume",
		["repeat"] = "repeat",
		["random"] = "random",
		["single"] = "single",
		["consume"] = "consume",
		["playlistlength"] = "playlist_length",
		["state"] = "state",
		["song"] = "song",
		["elapsed"] = "elapsed",
		["bitrate"] = "bitrate",
		["nextsong"] = "next_song",
		["file"] = "file",
		["Date"] = "date",
		["Album"] = "album",
		["Title"] = "title",
		["Artist"] = "artist",
		["Genre"] = "genre",
		["Time"] = "duration"
	}
	local tbl = parse.keyval(str, "([%w]*): (.*)", replaces)
	return tbl
end

local function connect(str, ret)
	local string = str or ""
	local conf = octavia.config
	local cmd = "echo '"
	if conf.pass then
		cmd = cmd .. "password " .. conf.pass .. "\n"
	end
	cmd = cmd .. string .. "\nclose'"
	cmd = cmd .. " | curl --connect-timeout 1 --max-time 1 -s telnet://"
	cmd = cmd .. conf.host or "localhost"
	cmd = cmd .. ":" .. conf.port
	local f = io.popen(cmd)
	local out = parser(f:read("*all"))
	f:close()
	if ret and out or parse.tbl_getn(out) ~= 0 then
		return out
	end
end

local function update()
	local oldsong, newsong, picon
	oldsong = octavia.data["title"]
	octavia.data = connect("status\ncurrentsong", true)
	for k, v in pairs(_nodata) do
		if octavia.data[k] == nil or octavia.data[k] == "" then
			octavia.data[k] = v
		end
	end
	newsong = octavia.data["title"]
	local coverdir = cachedir .. octavia.data["artist"]
	local cover = coverdir .. "/" .. octavia.data["album"] .. ".png"
	if not util.file_readable(cover) then
		if octavia.data["artist"] ~= "Unknown" or octavia.data["album"] ~= "Unknown" then
			spawn(configdir .. "scripts/cover-fetcher.sh '" .. octavia.data["artist"] .. "' '" .. octavia.data["album"] .. "' &")
		end
		cover = theme.octavia["nocover"]
	end
	if _initialized and oldsong ~= newsong and not w.d["wibox"].visible then
		notify({
			icon = cover,
			icon_size = 80,
			title = octavia.data["title"],
			text = "By <b>" .. octavia.data["artist"] .. "</b>\n"
			.. "From <b>" .. octavia.data["album"] .. "</b>\n"
			.. "Next song: #" .. ( octavia.data["next_song"] + 1 ) .. ""
		})
	end

	-- panel
	w.p["text"].text = octavia.data["title"]
	if octavia.data["state"] == "play" then
		picon = "pause"
	elseif octavia.data["state"] == "stop" or octavia.data["state"] == "pause" then
		picon = "play"
	else
		picon = "disconnect"
	end
	w.p["toggle"].image = theme.octavia.onpanel[picon]
	w.d["toggle"].image = theme.octavia.widget[picon]

	-- desktop
	if w.d["wibox"] and w.d["wibox"].visible then
		for _, i in pairs({ "repeat", "random", "single", "consume" }) do
			if octavia.data[i] == "1" then
				w.d[i].opacity = 1
			elseif octavia.data[i] == "0" then
				w.d[i].opacity = 0.25
			end
		end
		if _volume.muted then
			w.d["volicon"].opacity = 0.25
		else
			w.d["volicon"].opacity = 1
		end
		w.d["volbar"].value = tonumber(octavia.data["volume"])
		if not _volume.dontupdate then
			w.d["volume"].value = tonumber(octavia.data["volume"])
		end
		if not _seek_dontupdate then
			local _dur
			if tonumber(octavia.data["duration"]) == 0 then
				_dur = 1
			else
				_dur = tonumber(octavia.data["duration"])
			end
			w.d["seek"].maximum = _dur
			w.d["seek"].value = tonumber(octavia.data["elapsed"])
		end
		for _, t in pairs({ "elapsed", "duration" }) do
			w.d[t].text = parse.to_time(octavia.data[t])
		end
		for _, t in pairs({  "title", "album", "artist" }) do
			w.d[t].text = octavia.data[t]
		end
		w.d["playlist"].text = "#" .. math.floor(octavia.data["song"] + 1) .. "/" .. octavia.data["playlist_length"]
		w.d["bitrate"].text = octavia.data["bitrate"] .. " kbps"
		w.d["vol"].text = "vol: " .. octavia.data["volume"] .. "%"
	end

	--
	w["cover"].image = cover
	w["bar"].max_value = tonumber(octavia.data["duration"])
	w["bar"].value = tonumber(octavia.data["elapsed"])
end

local function worker()
	local t = timer(octavia.config.timeout, function()
		update()
		return true
	end)
	t:again()
	t:emit_signal("timeout")
end

function octavia:control(action)
	if not action or action == "" then return nil end
	local state = octavia.data["state"]
	if not state or state == "" then return nil end
	if action == "toggle" then
		if state == "stop" or state == "pause" then
			action = "play"
		elseif state == "play" then
			action = "pause"
		end
	end
	for _, i in pairs({ "repeat", "random", "single", "consume" }) do
		if action == i then
				action = action .. " " .. tonumber( ( octavia.data[i] + 1 ) %2 )
		end
	end
	connect(action, false)
	update()
end

function octavia.init(args)
	args = args or {}
	if not args.timeout or tonumber(args.timeout) == nil or args.timeout <= 0 then
		args.timeout = _defaults.timeout
	end
	if not args.host then args.host = _defaults.host end
	if not args.port or tonumber(args.port) == nil then
		args.port = _defaults.port
	else
		args.port = tostring(args.port)
	end
	if not args.pass or args.pass == "" then args.pass = nil end
	if not args.player or args.player == "" then args.player = _defaults.player end
	octavia.config = args

	-- panel
	w.p["text"] = wibox.widget.textbox()
	w.p["text"].font = theme.octavia.onpanel["font"]
	w.p["show"] = wibox.widget.imagebox(theme.octavia.onpanel["show"], false)
	w.p["show"]:buttons(table_join(
		button({}, 1, function()
			octavia:toggle_widget()
		end)
	))

	for _, i in pairs({ "toggle", "previous", "next" }) do
		w.p[i] = wibox.widget.imagebox(theme.octavia.onpanel[i], false)
		w.p[i]:buttons(table_join(
			button({}, 1, function()
				octavia:control(i, false)
			end)
		))
		w.d[i] = wibox.widget.imagebox(theme.octavia.widget[i], false)
		w.d[i]:buttons(table_join(
			button({}, 1, function()
				octavia:control(i, false)
			end)
		))
	end
	-- desktop
	for _, i in pairs({ "stop", "repeat", "random", "single", "consume" }) do
		w.d[i] = wibox.widget.imagebox(theme.octavia.widget[i], false)
		w.d[i]:buttons(table_join(
			button({}, 1, function()
				octavia:control(i, false)
			end)
		))
	end
	for _, s in pairs({ "volume", "seek" }) do
		w.d[s] = wibox.widget {
			bar_height = 19,
			bar_border_width = 0,
			bar_color = "#00000000",
			handle_color = "#FFFFFF",
			handle_shape = shape.circle,
			handle_border_width = 0,
			handle_border_color = "#00000000",
			forced_height = 19,
			handle_width = 9,
			value = 0,
			minimum = 0,
			maximum = 100,
			widget = wibox.widget.slider,
		}
	end
	for _, t in pairs({ "elapsed", "duration", "bitrate", "playlist", "vol", "title", "album", "artist" }) do
		w.d[t] = wibox.widget.textbox()
		w.d[t].font = theme.octavia.widget["small_font"]
	end
	for _, c in pairs({ "title", "album", "artist" }) do
		w.d[c].align = "center"
		w.d[c].font = theme.octavia.widget["font"]
		w.d[c].forced_height = 18
	end
	w.d["player"] = wibox.widget.imagebox(theme.octavia.widget["player"], false)
	w.d["player"]:buttons(table_join(
		button({}, 1, function()
			octavia:toggle_widget()
			octavia:player()
		end)
	))
	w.d["title"].font = theme.octavia.widget["big_font"]
	w.d["playlist"].align = "center"
	w.d["playlist"].font = theme.octavia.widget["small_font"]
	w.d["overlay"] = wibox.widget.imagebox(theme.octavia.widget["overlay"])
	w.d["volicon"] = wibox.widget.imagebox(theme.octavia.widget["volicon"], false)
	w.d["volicon"]:buttons(table_join(
		button({}, 1, function()
			local v, i
			if _volume.muted then
				i, v = "+", _volume.saved_volume
				_volume.muted = false
			else
				i, v = "-", octavia.data["volume"]
				_volume.saved_volume, _volume.muted = v, true
			end
			octavia:control("volume " .. i .. v, false)
		end),
		button({}, 4, function()
			octavia:control("volume +1", false)
		end),
		button({}, 5, function()
			octavia:control("volume -1", false)
		end)
	))
	w.d["seek"]:connect_signal("button::press", function()
		_seek_dontupdate = true
	end)
	w.d["seek"]:connect_signal("button::release", function(o)
		octavia:control("seek " .. octavia.data["song"] .. " " .. o.value)
		_seek_dontupdate = false
		update()
	end)
	w.d["volume"]:connect_signal("button::press", function()
		_volume.dontupdate = true
	end)
	w.d["volume"]:connect_signal("button::release", function(o)
		octavia:control("volume " .. (o.value - octavia.data.volume))
		_volume.dontupdate = false
		update()
	end)
	w.d["volbar"] = wibox.widget {
		widget = wibox.widget.progressbar,
		forced_height = 1,
		color = "#FFFFFF",
		background_color = "#202020",
		max_value = 100
	}
	--
	w["bar"] = wibox.widget {
		widget = wibox.widget.progressbar,
		forced_height = 1,
		color = "#FFFFFF",
		background_color = "#202020"
	}
	w["cover"] = wibox.widget.imagebox(nil, true)

	worker()
	_initialized = true
end

function octavia:toggle_widget()
	local s_geom = screen[mouse.screen].workarea
	local screen_w = s_geom.x + s_geom.width
	w.d["wibox"].visible = not w.d["wibox"].visible
	if w.d["wibox"].visible then
		w.d["wibox"]:geometry({
			x = ( ( screen_w - 250 ) / 2 ),
			y = s_geom.y
		})
		update()
	end
end

function octavia:player()
	local matcher = function(c)
		return match(c, { class = "Player" })
	end
	run_or_raise(octavia.config.player, matcher)
end

function octavia.widget()
	w.d["wibox"] = wibox({
		visible = false,
		ontop = true,
		width = 250,
		height = 250,
		fg = "#FFFFFF",
		bg = "#000000",
		type = "dock"
	})
	w.d["wibox"]:setup {
		layout = wibox.layout.stack,
		w["cover"],
		w.d["overlay"],
		{ layout = wibox.layout.align.vertical,
			{ layout = wibox.container.margin,
				left = 3, right = 3,
				{ layout = wibox.layout.fixed.vertical,
					{ layout = wibox.layout.align.horizontal,
						w.d["volicon"],
						{ layout = wibox.layout.stack,
							wibox.container.margin(w.d["volbar"],7,7,9,9),
							wibox.container.margin(w.d["volume"],3,3,0,0)
						},
						{ layout = wibox.layout.fixed.horizontal,
							w.d["repeat"], w.d["random"], w.d["single"], w.d["consume"]
						}
					},
					{ layout = wibox.container.margin,
						left = 5, right = 5,
						{ layout = wibox.layout.stack,
							wibox.container.margin(w.d["playlist"],4,4,0,0),
							{ layout = wibox.layout.align.horizontal,
								w.d["vol"],
								nil,
								w.d["bitrate"]
							}
						}
					}
				}
			},
			nil,
			{ layout = wibox.layout.fixed.vertical,
				{ layout = wibox.container.margin,
					left = 8, right = 8,
					{ layout = wibox.layout.fixed.vertical,
						w.d["title"], w.d["album"], w.d["artist"]
					}
				},
				{ layout = wibox.layout.stack,
					{ layout = wibox.container.margin,
						left = 8, right = 8,
						{ layout = wibox.layout.align.horizontal,
							w.d["elapsed"], nil, w.d["duration"]
						}
					},
					{ layout = wibox.container.margin,
						left = math.floor((250 - ((31*3)+(21*2))) /2),
						right = math.floor((250 - ((31*3)+(21*2))) /2),
						{ layout = wibox.layout.fixed.horizontal,
							w.d["previous"], w.d["stop"], w.d["toggle"], w.d["player"], w.d["next"]
						}
					}
				},
				{ layout = wibox.container.margin,
					left = 5, right = 5,
					{ layout = wibox.layout.stack,
						wibox.container.margin(w["bar"],4,4,9,9),
						w.d["seek"]
					}
				}
			}
		}
	}
	-- onpanel
	local onpanel = wibox.widget {
		layout = wibox.container.constraint,
		strategy = "exact",
		width = 200,
		{ layout = wibox.layout.fixed.vertical,
			{ layout = wibox.layout.align.horizontal,
				{ layout = wibox.layout.fixed.horizontal,
					w.p["previous"], w.p["toggle"], w.p["next"]
				},
				wibox.container.margin(w.p["text"],2,2,0,0),
				w.p["show"],
			},
			wibox.container.margin(w["bar"],1,1,0,1)
		},
	}
	return onpanel
end

return octavia
