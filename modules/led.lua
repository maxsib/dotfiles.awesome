local wibox = require("wibox")
local theme = require("beautiful").get()
local timer = require("gears.timer").start_new

local led = { mt = {} }
local w = {}
local leds = { "caps", "num", "scroll" }
for _, l in pairs(leds) do
	w[l] = wibox.widget.imagebox(theme.led,false)
end

local function update(input)
	for _, l in pairs(leds) do
		local f = io.open("/sys/class/leds/input" .. input .. "::" .. l .. "lock/brightness")
		local state = f:read("*all")
		f:close()
		w[l]:set_image(theme.led) -- widget not updated without this
		if tonumber(state) == 0 then
			w[l]:set_opacity(0.35)
		elseif tonumber(state) == 1 then
			w[l]:set_opacity(1)
		end
	end
end

local function worker(input)
	local t = timer(0.5, function()
		update(input)
		return true
	end)
	t:again()
	t:emit_signal("timeout")
end

function led.new(input)
	if input == nil or input == "" or tonumber(input) == nil then
		input = 0
	end
	worker(input)
	local widget = wibox.widget {
		{
			w.caps,
			w.num,
			w.scroll,
			layout = wibox.layout.fixed.vertical
		},
		top = 6,
		bottom = 6,
		left = 2,
		right = 2,
		layout = wibox.container.margin
	}
	return widget
end

local _instance = nil

function led.mt:__call(...)
	if _instance == nil then
		_instance = led.new(...)
	end
	return _instance
end

return setmetatable(led, led.mt)
