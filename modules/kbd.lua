local capi = { awesome = awesome }

local theme = require("beautiful").get()
local textbox = require("wibox.widget.textbox")
local widget_base = require("wibox.widget.base")
local table_join = require("awful.util").table.join
local button = require("awful.button")

local kbd = { mt = {} }

local function get_groups_from_group_names(group_names)
	local langs = { "ad", "af", "al", "am", "ara", "at", "az", "ba", "bd", "be",
		"bg", "br", "bt", "bw", "by", "ca", "cd", "ch", "cm", "cn", "cz", "de",
		"dk", "ee", "epo", "es", "et", "fi", "fo", "fr", "gb", "ge", "gh", "gn",
		"gr", "hr", "hu", "ie", "il", "in", "iq", "ir", "is", "it", "jp", "ke",
		"kg", "kh", "kr", "kz", "la", "latam", "latin", "lk", "lt", "lv", "ma",
		"mao", "me", "mk", "ml", "mm", "mn", "mt", "mv", "ng", "nl", "no", "np",
		"ph", "pk", "pl", "pt", "ro", "rs", "ru", "se", "si", "sk", "sn", "sy",
		"th", "tj", "tm", "tr", "tw", "tz", "ua", "us", "uz", "vn", "za"
	}
	local tokens = {}
	string.gsub(group_names, "[^+]+", function(match)
		table.insert(tokens, match)
	end)
	local layouts = {}
	local ends = { "$", "%(" }
	for _, token in pairs(tokens) do
		for _, match in pairs(langs) do
			for _, matchend in pairs(ends) do
				local name = string.match(token, "^" .. match .. matchend)
				if name then name = name:gsub("%(", "") end
				table.insert(layouts, name)
			end
		end
	end
	return layouts
end

local function update_status(self)
	self._current = awesome.xkb_get_layout_group()
	local text = ""
	if #self._layout > 0 then
		text = self._layout[self._current + 1]
	end
	local color = theme.kbd_fg or theme.fg_normal
	self.widget:set_markup("<span color='" .. color .. "'>" .. text .. "</span>")
end

local function update_layout(self)
	self._layout = {}
	local layouts = get_groups_from_group_names(awesome.xkb_get_group_names())
	if layouts == nil or layouts[1] == nil then
		error("Failed to get list of keyboard groups")
		return
	end
	for k, v in pairs(layouts) do
		local layout_name = self.layout_name(v)
		self._layout[k] = layout_name
	end
	update_status(self)
end

function kbd.new()
	local widget = textbox()
	widget:set_font(theme.kbd_font or theme.font)
	local self = widget_base.make_widget(widget)
	self.widget = widget
	self.layout_name = function(v)
		local name
		if theme.kbd_replacements[v] then
			name = theme.kbd_replacements[v]
		else
			name = v
		end
		return name
	end
	self.next_layout = function()
		self.set_layout((self._current + 1) % (#self._layout + 1))
	end
	self.prev_layout = function()
		self.set_layout((self._current - 1) % #self._layout)
	end

	self.set_layout = function(group_number)
		if 0 > group_number or group_number > #self._layout then
			error("Invalid group number: " .. group_number .. "expected number from 0 to " .. #self._layout)
			return;
		end
		awesome.xkb_set_layout_group(group_number);
	end

	update_layout(self);
	capi.awesome.connect_signal("xkb::map_changed", function() update_layout(self) end)
	capi.awesome.connect_signal("xkb::group_changed", function() update_status(self) end)
	self:buttons(table_join(
		button({}, 1, self.next_layout),
		button({}, 3, self.prev_layout)
	))
	return self
end

local _instance = nil

function kbd.mt:__call(...)
	if _instance == nil then
		_instance = kbd.new(...)
	end
	return _instance
end

return setmetatable(kbd, kbd.mt)
