pcall(function() jit.on() end)
local gears = require("gears")
local awful = require("awful")
require("awful.autofocus")
local wibox = require("wibox")
local beautiful = require("beautiful")
local naughty = require("naughty")
local hotkeys_popup = require("awful.hotkeys_popup").widget

beautiful.init(awful.util.get_configuration_dir() .. "themes/nightmare/theme.lua")

local m = {}
m.upd_func = require("modules.upd_func")
m.cal = require("modules.calendar")
m.helpers = require("modules.helpers")
m.kbd = require("modules.kbd")
m.mail = require("modules.maildir")

local cicon = require("modules.cicon")
local ups = require("modules.ups").init("powercom@localhost")
local led = require("modules.led")
local octavia = require("modules.octavia")

octavia.init({timeout = 1})

-- {{{ NAUGHTY CONFIG
naughty.config.defaults = {
	timeout = 10,
	ontop = true,
	margin = 8,
	border_width = 0,
	position = "top_right",
	bg = beautiful.color.black,
	fg = beautiful.color.white,
	icon_size = 32,
}
naughty.config.padding = 8
naughty.config.spacing = 0
naughty.config.bg = beautiful.color.black
naughty.config.fg = beautiful.color.white
naughty.config.border_width = 0
naughty.config.position = "top_center"
naughty.config.ontop = true
naughty.config.presets.critical.bg = beautiful.color.black
naughty.config.presets.critical.fg = beautiful.color.mark
naughty.config.presets.low.icon = beautiful.icon.notify.i
naughty.config.presets.normal.icon = beautiful.icon.notify.i
naughty.config.presets.critical.icon = beautiful.icon.notify.e
-- }}}

-- {{{ ERROR HANDLING
if awesome.startup_errors then
	naughty.notify({
		preset = naughty.config.presets.critical,
		title = "Oops, there were errors during startup!",
		text = awesome.startup_errors
	})
end
do
	local in_error = false
	awesome.connect_signal("debug::error", function(err)
		if in_error then return end
		in_error = true
		naughty.notify({
			preset = naughty.config.presets.critical,
			title = "Oops, an error happened!",
			text = tostring(err)
		})
		in_error = false
	end)
end
-- }}}

-- {{{ VARIABLES

local terminal = "/usr/bin/st"
local sudo = "/usr/bin/ktsuss -u root "

local modkey = "Mod4"

awful.layout.layouts = {
	awful.layout.suit.fair,
	awful.layout.suit.tile.left,
	awful.layout.suit.tile,
	awful.layout.suit.max,
	awful.layout.suit.floating
}
-- }}}

-- {{{ HELPER FUNCTIONS
local panels = {}
function panels:get_state(s)
	s = s or client.screen or mouse.screen or 1
	if self[s] == nil then self[s] = {} end
	if self[s].visible == nil then
		self[s].visible = true
	end
	return self[s].visible
end
function panels:set(state, persist, s)
	s = s or client.screen or mouse.screen or 1
	s.panel.visible = state
	s.side.visible = state
	if persist then
		self[s].visible = state
	end
end
function panels:restore(s)
	s = s or client.screen or mouse.screen or 1
	local oldstate = self:get_state(s)
	self:set(oldstate, false, s)
end
function panels:toggle(persist, s)
	s = s or client.screen or mouse.screen or 1
	local oldstate = self:get_state(s)
	self:set(not oldstate, persist, s)
end

local function client_menu()
	local instance = nil
	return function()
		if instance and instance.wibox.visible then
			instance:hide()
			instance = nil
		else
			instance = awful.menu.clients({ theme = { width = 250 }}, {}, function(c)
				if c.class then
					return true
				end
			end)
		end
		end
end
-- }}}

-- {{{ Menu
local _mb = {
	l = awful.menu({
		items = {
			{ "Internet", {
				{ "Qutebrowser", "/usr/bin/qutebrowser", m.helpers.icon("browser") },
				{ "Deluge", "/usr/bin/deluge", m.helpers.icon("deluge") },
				{ "Poezio", "/mnt/sdb1/bin/poezio", m.helpers.icon("im-jabber") }
				-- { "E-Mail", terminal .. " -c 'Mutt' -e '/usr/bin/mutt'", m.helpers.icon("e-mail") }
				-- { "RSS Reader", terminal .. " -c 'NewsBeuter' -e '/usr/bin/newsbeuter'", m.helpers.icon("internet-news-reader") }
			}, m.helpers.icon("applications-internet") },
			{ "Utilities", {
				{ "NeoVim", terminal .. " -c 'Neovim' -e '/usr/bin/nvim'", m.helpers.icon("vim") },
				{ "File Manager", terminal .. " -c 'Ranger' -e '/usr/bin/ranger'", m.helpers.icon("file-manager") }
			}, m.helpers.icon("applications-utilities") },
			{ "Multimedia", {
				{ "GIMP", "/usr/bin/gimp", m.helpers.icon("gimp") },
				{ "Player", function() octavia:player() end, m.helpers.icon("music-player") },
				{ "ALSA Mixer", terminal .. " -c 'Alsamixer' -t 'AlsaMixer' -e '/usr/bin/alsamixer'", m.helpers.icon("alsa-tools") },
				{ "SunVOX", "/mnt/sdb1/bin/sunvox", m.helpers.icon("executable") }
			}, m.helpers.icon("applications-multimedia") },
			{ "Settings", {
				{ "Root", {
					{ "GParged", sudo .. "/usr/bin/gparted-pkexec", m.helpers.icon("gparted") },
					{ "HTop", sudo .. terminal .. " -c 'Htop' -t 'HTop' -e '/usr/bin/htop'", m.helpers.icon("htop") },
					{ "Terminal", sudo .. terminal, m.helpers.icon("terminal") },
					{ "NeoVim", sudo .. terminal .. " -c 'Neovim' -e '/usr/bin/nvim'", m.helpers.icon("vim") },
					{ "Ranger", sudo .. terminal .. " -c 'Ranger' -e '/usr/bin/ranger'", m.helpers.icon("file-manager") }
				}, m.helpers.icon("utilities-terminal-root") },
				{ "Helpers", {
					{ "HUP xsettingsd", "/usr/bin/killall -s HUP xsettingsd", m.helpers.icon("reload") },
					{ "Get client info", function() m.helpers.client_info() end, m.helpers.icon("dialog-information") },
					{ "Kill window", function() m.helpers.killer() end, m.helpers.icon("process-stop") },
					{ "Hotkeys Help", function() return false, hotkeys_popup.show_help end, m.helpers.icon("keyboard") },
					{ "awesome version", function()
						naughty.notify({ timeout = 0, title = "awesome wm version:", text = awesome.version })
					end, m.helpers.icon("dialog-information") }
				}, m.helpers.icon("help-contents") },
				{ "HTop", terminal .. " -c 'Htop' -t 'HTop' -e '/usr/bin/htop'", m.helpers.icon("htop") },
				{ "Terminal", terminal, m.helpers.icon("terminal") },
				{ "GTK+2 Theme", "/usr/bin/lxappearance", m.helpers.icon("preferences-desktop-theme") },
				{ "Qt5 Config", "/usr/bin/qt5ct", m.helpers.icon("preferences-desktop-theme") }
			}, m.helpers.icon("applications-system") },
			{ "Session", {
				{ "Lock", "xautolock -locknow", m.helpers.icon("lock") },
				{ "Restart", awesome.restart, m.helpers.icon("reload") },
				{ "Quit", function() awesome.quit() end, m.helpers.icon("application-exit") },
				{ "Suspend", "systemctl suspend", m.helpers.icon("system-suspend") },
				{ "Hibernate", "systemctl hibernate", m.helpers.icon("system-hibernate") },
				{ "Reboot", "systemctl reboot", m.helpers.icon("system-reboot") },
				{ "Shutdown", "systemctl poweroff", m.helpers.icon("system-shutdown") }
			}, m.helpers.icon("power-management") }
		}
	}),
	i = wibox.widget.imagebox(beautiful.icon.menu),
}
local menubutton = wibox.container.background(_mb.i)

menubutton:connect_signal("mouse::enter", function()
	menubutton:set_bg(beautiful.color.normal)
end)
menubutton:connect_signal("mouse::leave", function()
	menubutton:set_bg()
end)

-- m.dash = require("modules.dash")
menubutton:buttons(awful.util.table.join(
	awful.button({}, 1, function()
		_mb.l:toggle({ coords = { x = 0, y = 0 }})
	end),
	awful.button({}, 3, function()
		-- m.dash:open()
	end)
))
-- }}}

-- {{{ Wibar
-- local mykeyboardlayout = awful.widget.keyboardlayout()
local mykeyboardlayout = m.kbd()

-- {{{ SYSTRAY
local _st = {
	mrg = wibox.layout.fixed.horizontal(),
	i = wibox.widget.imagebox(beautiful.icon.tray.closed, false),
	b = wibox.container.background(),
	wtf = wibox({
		y = -1010,
		width = 10,
		visible = false,
		ontop = false
	}),
	visible = false
}

_st.mrg:add(_st.b)
_st.mrg:add(wibox.container.margin(_st.i,4,0,0,0))

local systray = wibox.container.background()
systray:set_widget(wibox.container.margin(_st.mrg,0,0,0,0))
_st.wtf:set_widget(wibox.widget.systray())
_st.b:set_bg(beautiful.bg_systray)

function systray:toggle()
	if not _st.visible then
		_st.visible = true
		_st.wtf:set_widget()
		self:set_bg(beautiful.bg_systray)
		self:set_bgimage(beautiful.bgimage_systray)
		_st.b:set_widget(wibox.container.margin(wibox.widget.systray(),4,0,4,4))
		_st.i:set_image(beautiful.icon.tray.opened)
	else
		_st.visible = false
		self:set_bg() -- = beautiful.color.none
		self:set_bgimage()
		_st.b:set_widget()
		_st.wtf:set_widget(wibox.widget.systray())
		_st.i:set_image(beautiful.icon.tray.closed)
	end
end

_st.i:buttons(awful.util.table.join(
	awful.button({}, 1, function() systray:toggle() end)
))
-- }}}

local _t = {}
_t.w = wibox.widget {
	widget = wibox.widget.progressbar,
	ticks = true,
	ticks_gap = 1,
	ticks_size = 4,
	forced_height = 1,
	forced_width = 34,
	background_color = "#202020",
	color = beautiful.timewidget.color,
	max_value = 7
}
gears.timer.start_new(1, function()
	_t.w.value = tonumber(os.date("%u"))
	return true
end)

local timewidget = wibox.widget {
	layout = wibox.container.margin,
	left = 6, right = 6,
	{
		layout = wibox.layout.align.vertical,
		nil,
		{
			layout = wibox.container.margin,
			wibox.widget.textclock(
				"<span font='"
				.. beautiful.timewidget.font
				.. "' color='"
				.. beautiful.timewidget.color
				.. "'>%H:%M</span>",
				1
			),
		},
		{
			layout = wibox.container.margin,
			top = -2, bottom = 1, left = 3, right = 3,
			_t.w
		}
	}
}

timewidget:buttons(awful.util.table.join(
	awful.button({}, 1, function()
		m.cal:toggle(0)
	end)
))

local taglist_buttons = awful.util.table.join(
	awful.button({}, 1, function(t) t:view_only() end),
	awful.button({ modkey }, 1, function(t)
		if client.focus then
			client.focus:move_to_tag(t)
		end
	end),
	awful.button({}, 3, awful.tag.viewtoggle),
	awful.button({ modkey }, 3, function(t)
		if client.focus then
			client.focus:toggle_tag(t)
		end
	end),
	awful.button({}, 5, function(t) awful.tag.viewnext(t.screen) end),
	awful.button({}, 4, function(t) awful.tag.viewprev(t.screen) end)
)

local tasklist_buttons = awful.util.table.join(
	awful.button({}, 1, function(c)
		if c == client.focus then
			c.minimized = true
		else
			c.minimized = false
			if not c:isvisible() and c.first_tag then
				c.first_tag:view_only()
			end
			client.focus = c
			c:raise()
		end
	end),
	awful.button({}, 3, client_menu()),
	awful.button({}, 4, function()
		awful.client.focus.byidx(1)
	end),
	awful.button({}, 5, function()
		awful.client.focus.byidx(-1)
	end)
)

local function set_wallpaper(s)
	if beautiful.wallpaper then
		local wallpaper = beautiful.wallpaper
		if type(wallpaper) == "function" then
			wallpaper = wallpaper(s)
		end
		gears.wallpaper.maximized(wallpaper, s, false)
	end
end
screen.connect_signal("property::geometry", set_wallpaper)
local layout = awful.layout.layouts
local tags = {
	settings = {
		{
			names = { "dev", "term", "net", "mail", "media", "etc" },
			layouts = { layout[1], layout[1], layout[2], layout[3], layout[4], layout[5] },
			icons = { beautiful.icon.tag.dev, beautiful.icon.tag.term, beautiful.icon.tag.net, beautiful.icon.tag.mail, beautiful.icon.tag.media, beautiful.icon.tag.etc }
		},
	}
}
awful.screen.connect_for_each_screen(function(s)
	set_wallpaper(s)

	tags[s] = awful.tag(tags.settings[s.index].names, s, tags.settings[s.index].layouts)
	for i, _ in pairs(tags[s]) do
		for k, v in pairs({
			icon_only = true,
			icon = tags.settings[s.index].icons[i],
		}) do
			awful.tag.setproperty(tags[s][i], k, v)
		end
	end
	s.mypromptbox = awful.widget.prompt()
	s.mylayoutbox = awful.widget.layoutbox(s)
	s.mylayoutbox:buttons(awful.util.table.join(
		awful.button({}, 1, function() awful.layout.inc(1) end),
		awful.button({}, 3, function() awful.layout.inc(-1) end),
		awful.button({}, 5, function() awful.layout.inc(1) end),
		awful.button({}, 4, function() awful.layout.inc(-1) end)
	))

	s.mytaglist = awful.widget.taglist(
		s,
		awful.widget.taglist.filter.all,
		taglist_buttons,
		{},
		m.upd_func.tags
	)

	s.mytasklist = awful.widget.tasklist(
		s,
		awful.widget.tasklist.filter.alltags,
		tasklist_buttons,
		{},
		m.upd_func.tasks,
		wibox.layout.fixed.vertical()
	)

	s._ci = {
		i = wibox.widget.imagebox(),
		t = wibox.widget.textbox(),
		f = wibox.layout.fixed.horizontal()
	}
	s._ci.f:add(wibox.container.margin(s._ci.i,4,2,4,4))
	s._ci.f:add(wibox.container.margin(s._ci.t,2,4,4,4))

	s.cinfo = wibox.container.constraint()
	s.cinfo:set_widget(s._ci.f)
	s.cinfo:set_strategy("max")
	s.cinfo:set_width(800)

	s.panel = awful.wibar({
		position = "top",
		screen = s,
		height = 24,
		ontop = true,
		bg = beautiful.color.black --none
	})
	s.panel:setup {
		layout = wibox.layout.align.horizontal,
		{
			layout = wibox.layout.fixed.horizontal,
			menubutton,
			wibox.container.margin(s.mytaglist, 4,4,0,0),
			s.mylayoutbox,
			s.mypromptbox,
			s.cinfo
		},
		-- s.mytasklist,
		nil,
		{
			layout = wibox.layout.fixed.horizontal,
			systray,
			wibox.container.margin(octavia.widget(),4,4,0,0),
			ups,
			m.mail({ dir = "/home/r3lgar/.mail/r3lgar-openmailbox", timeout = (1*60) }),
			mykeyboardlayout,
			led(),
			timewidget,
		},
	}
	s.side = awful.wibar({
		position = "left",
		screen = s,
		width = 40,
		ontop = true,
		bg = beautiful.color.none
	})
	s.side:setup {
		layout = wibox.layout.align.vertical,
		-- expand = "outside",
		{
			layout = wibox.layout.fixed.vertical,
			s.mytasklist
		},
		nil,
		{
			layout = wibox.layout.fixed.vertical,
			nil
		}
	}
end)
-- }}}

-- {{{ KEY BINDINGS
-- {{{ GLOBALKEYS
local globalkeys = awful.util.table.join(
-- {{{ AWESOME KEYS
	awful.key({ modkey, "Control" }, "r", awesome.restart,
		{ group = "awesome", description = "Restart awesome" }),
	awful.key({ modkey, "Shift" }, "q", awesome.quit,
		{ group = "awesome", description = "Exit awesome" }),
	awful.key({ modkey }, "s", hotkeys_popup.show_help,
		{ group = "awesome", description = "Show hotkeys help" }),
	awful.key({ modkey }, "b", function() panels:toggle(true) end,
		{ group = "awesome", description = "Toggle panel visibility" }),
	awful.key({ modkey }, "v", function() systray:toggle() end,
		{ group = "awesome", description = "Toggle system tray visibility" }),
	awful.key({ modkey }, "d", function() m.cal:toggle(0) end,
		{ group = "awesome", description = "Toggle calendar visibility" }),
	awful.key({ modkey }, "l", function() awful.spawn.with_shell("/usr/bin/xautolock -locknow") end,
		{ group = "awesome", description = "Lock screen" }),
-- }}}

-- {{{ SCREEN KEYS
	awful.key({ modkey, "Shift" }, "n", function() awful.screen.focus_relative(1) end,
		{ group = "screen", description = "Focus the next screen" }),
	awful.key({ modkey, "Shift" }, "p", function() awful.screen.focus_relative(-1) end,
		{ group = "screen", description = "Focus the previous screen" }),
-- }}}

-- {{{ LAUNCHERS
	awful.key({ modkey }, "\\", function() awful.spawn.with_shell("/mnt/sdb1/bin/passmenu2") end,
		{ group = "launcher", description = "Password menu" }),
	awful.key({ modkey }, "w", function() _mb.l:show() end,
		{ group = "launcher", description = "Show main menu" }),
	awful.key({ modkey }, "Return", function() awful.spawn(terminal) end,
		{ group = "launcher", description = "Open a terminal" }),

	awful.key({ modkey }, "r",
		function()
			awful.prompt.run {
				font = beautiful.monofont,
				prompt = "Run: ",
				textbox = awful.screen.focused().mypromptbox.widget,
				exe_callback = awful.spawn.with_shell,
				history_path = awful.util.get_cache_dir() .. "/history"
			}
		end,
		{ group = "launcher", description = "Run prompt" }),
	awful.key({ modkey, "Shift" }, "x",
		function()
			awful.prompt.run {
				font = beautiful.monofont,
				prompt = "Lua: ",
				textbox = awful.screen.focused().mypromptbox.widget,
				exe_callback = awful.util.eval,
				history_path = awful.util.get_cache_dir() .. "/history_eval"
			}
		end,
		{ group = "launcher", description = "Run Lua code" }),
	awful.key({ modkey }, "a",
		function()
			awful.prompt.run {
				font = beautiful.monofont,
				prompt = "Calc: ",
				textbox = awful.screen.focused().mypromptbox.widget,
				exe_callback = function(expr)
					if not expr or expr == "" then expr = "0" end
					local symbols = {
						["*"] = "×",
						["/"] = "÷"
					}
					local txt, tmo
					local result = awful.util.eval("return (" .. expr .. ")")
					if tostring(result) == "inf" then
						result = "∞"
					end
					expr = expr:gsub("[^%d.]", " %1 "):gsub("  ", " ")
					for k, v in pairs(symbols) do
						expr = expr:gsub(k, v)
					end
					if not result or result == "" then
						txt = "Invalid expression: «" .. expr .. "»"
						tmo = 2
					else
						txt = expr .. " = " .. result
						tmo = 0
					end
						naughty.notify({
							title = "Result:",
							text = txt,
							icon = beautiful.icon.notify.calc,
							timeout = tmo
						})
				end,
				history_path = awful.util.get_cache_dir() .. "/history_calc"
			}
		end,
		{ group = "launcher", description = "Calculator" }),
-- }}}

-- {{{ LAYOUTS
	awful.key({ modkey }, "l", function() awful.tag.incmwfact(0.025) end,
		{ group = "layout", description = "Increase master width factor" }),
	awful.key({ modkey }, "h", function() awful.tag.incmwfact(-0.025) end,
		{ group = "layout", description = "Decrease master width factor" }),

	awful.key({ modkey, "Shift" }, "h", function() awful.tag.incnmaster(1, nil, true) end,
		{ group = "layout", description = "Increase the number of master clients" }),
	awful.key({ modkey, "Shift" }, "l", function() awful.tag.incnmaster(-1, nil, true) end,
		{ group = "layout", description = "Decrease the number of master clients" }),

	awful.key({ modkey, "Control" }, "h", function() awful.tag.incncol(1, nil, true) end,
		{ group = "layout", description = "Increase the number of columns" }),
	awful.key({ modkey, "Control" }, "l", function() awful.tag.incncol(-1, nil, true) end,
		{ group = "layout", description = "Decrease the number of columns" }),

	awful.key({ modkey }, "space", function() awful.layout.inc(1) end,
		{ group = "layout", description = "Select next layout" }),
	awful.key({ modkey, "Shift" }, "space", function() awful.layout.inc(-1) end,
		{ group = "layout", description = "Select previous layout" }),
-- }}}

-- {{{ TAGS
	awful.key({ modkey }, "Left", awful.tag.viewprev,
		{ group = "tag", description = "View previous tag" }),
	awful.key({ modkey }, "Right", awful.tag.viewnext,
		{ group = "tag", description = "View next tag" }),
	awful.key({ modkey }, "Escape", awful.tag.history.restore,
		{ group = "tag", description = "Go back" }),
-- }}}

-- {{{ CLIENTS
	awful.key({ modkey }, "u", awful.client.urgent.jumpto,
		{ group = "client", description = "Jump to urgent client" }),
	awful.key({ modkey }, "j", function() awful.client.focus.byidx(1) end,
		{ group = "client", description = "Focus next client by index" }),
	awful.key({ modkey }, "k", function() awful.client.focus.byidx(-1) end,
		{ group = "client", description = "Focus previous client by index" }),
	awful.key({ modkey, "Shift" }, "j", function() awful.client.swap.byidx(1) end,
		{ group = "client", description = "Swap with next client by index" }),
	awful.key({ modkey, "Shift" }, "k", function() awful.client.swap.byidx(-1) end,
		{ group = "client", description = "Swap with previous client by index" }),

	awful.key({ modkey }, "Tab",
		function()
			awful.client.focus.history.previous()
			if client.focus then
				client.focus:raise()
			end
		end,
		{ group = "client", description = "Go back" }),

	awful.key({ modkey, "Control" }, "n",
		function()
			local c = awful.client.restore()
			if c then
				client.focus = c
				c:raise()
			end
		end,
		{ group = "client", description = "Restore minimized client" }),
-- }}}

-- {{{ MUSIC MANIPULATION
	awful.key({ modkey }, "Insert", function() octavia:control("toggle") end,
		{ group = "music", description = "Toggle playback" }),
	awful.key({ modkey }, "Delete", function() octavia:control("stop") end,
		{ group = "music", description = "Stop playback" }),
	awful.key({ modkey }, "Prior", function() octavia:control("previous") end,
		{ group = "music", description = "Previous song" }),
	awful.key({ modkey }, "Next", function() octavia:control("next") end,
		{ group = "music", description = "Next song" }),
	awful.key({ modkey }, "Home", function() octavia:control("volume +1") end,
		{ group = "music", description = "Increase volume" }),
	awful.key({ modkey }, "End", function() octavia:control("volume -1") end,
		{ group = "music", description = "Decrease volume" }),
	awful.key({ modkey }, "p", function() octavia:player() end,
		{ group = "music", description = "Run or raise player" }),
	awful.key({ modkey }, "g", function() octavia:toggle_widget() end,
		{ group = "music", description = "Show/hide octavia" })
-- }}}

)
-- }}}

-- {{{ TAGS ITERATION
for i = 1, 6 do
	globalkeys = awful.util.table.join(globalkeys,
		awful.key({ modkey }, "#" .. i + 9,
			function()
					local screen = awful.screen.focused()
					local tag = screen.tags[i]
					if tag then
					tag:view_only()
					end
			end,
			{ group = "tag", description = "View tag #" .. i }),
		awful.key({ modkey, "Control" }, "#" .. i + 9,
			function()
				local screen = awful.screen.focused()
				local tag = screen.tags[i]
				if tag then
					awful.tag.viewtoggle(tag)
				end
			end,
			{ group = "tag", description = "Toggle tag #" .. i }),
		awful.key({ modkey, "Shift" }, "#" .. i + 9,
			function()
				if client.focus then
					local tag = client.focus.screen.tags[i]
					if tag then
						client.focus:move_to_tag(tag)
					end
				end
			end,
			{ group = "tag", description = "Move focused client to tag #" .. i }),
		awful.key({ modkey, "Control", "Shift" }, "#" .. i + 9,
			function()
				if client.focus then
					local tag = client.focus.screen.tags[i]
					if tag then
						client.focus:toggle_tag(tag)
					end
				end
			end,
			{ group = "tag", description = "Toggle focused client on tag #" .. i })
	)
end
-- }}}

root.keys(globalkeys)

-- {{{ CLIENTKEYS
local clientkeys = awful.util.table.join(
	awful.key({ modkey }, "f",
		function(c)
			c.fullscreen = not c.fullscreen
			c:emit_signal("property::name")
			c:raise()
		end,
		{ group = "client", description = "Toggle fullscreen" }),
	awful.key({ modkey, "Shift" }, "c", function(c) c:kill() end,
		{ group = "client", description = "Close client" }),
	awful.key({ modkey, "Control" }, "space", function(c)
			-- awful.client.floating.toggle()
			c.floating = not c.floating
			c:emit_signal("property::name")
		end,
		{ group = "client", description = "Toggle floating" }),
	awful.key({ modkey, "Control" }, "Return", function(c) c:swap(awful.client.getmaster()) end,
		{ group = "client", description = "Move to master" }),
	awful.key({ modkey }, "o", function(c) c:move_to_screen() end,
		{ group = "client", description = "Move to screen" }),
	awful.key({ modkey }, "t",
		function(c)
			c.ontop = not c.ontop
			c:emit_signal("property::name")
		end,
		{ group = "client", description = "Toggle keep on top" }),
	awful.key({ modkey }, "n", function(c) c.minimized = true end ,
		{ group = "client", description = "Minimize current client" }),
	awful.key({ modkey }, "m",
		function(c)
			c.maximized = not c.maximized
			c:emit_signal("property::name")
			c:raise()
		end,
		{ group = "client", description = "Maximize current client" })
)
-- }}}
-- }}}

-- {{{ MOUSE BINDINGS
-- {{{ GLOBALBUTTONS
root.buttons(awful.util.table.join(
	awful.button({}, 3, function() _mb.l:toggle() end),
	awful.button({}, 5, awful.tag.viewnext),
	awful.button({}, 4, awful.tag.viewprev)
))
-- }}}

-- {{{ CLIENTBUTTONS
local clientbuttons = awful.util.table.join(
	awful.button({}, 1, function(c) client.focus = c; c:raise() end),
	awful.button({ modkey }, 1, awful.mouse.client.move),
	awful.button({ modkey }, 3, awful.mouse.client.resize)
)
-- }}}
-- }}}

-- {{{ RULES
awful.rules.rules = {
	{ rule = {},
		properties = {
			border_width = beautiful.border_width,
			border_color = beautiful.border_normal,
			focus = awful.client.focus.filter,
			raise = true,
			keys = clientkeys,
			buttons = clientbuttons,
			screen = awful.screen.preferred,
			-- size_hints_honor = false,
			placement = awful.placement.no_overlap + awful.placement.no_offscreen + awful.placement.centered,
		}
	},
	{ rule_any = {
			type = { "normal", "dialog" }
		},
		properties = { titlebars_enabled = false }
	},
	{ rule_any = {
			instance = {},
			role = {},
			type = { "dialog" },
			class = { "Pinentry-gtk-2" },
			name = { "Event Tester", "Web Inspector - *" }
		},
		properties = {
			floating = true,
			ontop = true
		},
	},
	{ rule_any = {
			class = { "Pinentry-gtk-2" },
			name = { "Event Tester" }
		},
		properties = {
			sticky = true,
		}
	},
	{ rule_any = {
			class = { "Neovim", "Ranger" }
		},
		properties = {
			screen = 1,
			tag = "dev",
			switchtotag = true
		}
	},
	{ rule_any = {
			instance = { "st-256color" }
		},
		properties = {
			size_hints_honor = false
		}
	},
	{ rule_any = {
			class = { "Termite", "st-256color" },
			name = { "Event Tester" }
		},
		properties = {
			screen = 1,
			tag = "term",
			switchtotag = true
		}
	},
	{ rule_any = {
			class = { "qutebrowser" }
		},
		properties = {
			screen = 1,
			tag = "net",
			switchtotag = true
		},
	},
	{ rule = {
			class = "qutebrowser"
		},
		except_any = {
			name = { "Web Inspector - *" },
			type = { "dialog" }
		},
		properties = {
			maximized = true
		}
	},
	{ rule_any = {
			class = { "Claws-mail", "Mutt", "NewsBeuter", "Poezio" }
		},
		properties = {
			screen = 1,
			tag = "mail",
			switchtotag = true
		},
		{ rule = {
				class = "Claws-mail",
				role = "mainwindow"
			},
			properties = {
				setmaster = true
			}
		},
	},
	{ rule_any = {
			class = { "Gimp", "mpv", "Player" }
		},
		properties = {
			screen = 1,
			switchtotag = true,
			tag = "media"
		}
	},
	{ rule_any = {
			class = {}
		},
		properties = {
			screen = 1,
			tag = "etc"
		}
	},
}
-- }}}

-- {{{ SIGNAL HELPERS
local sighelpers = {}

function sighelpers.fullscreen_hook(c)
	if c.fullscreen then
		panels:set(false, false, c.screen)
		awful.spawn.with_shell(awful.util.get_configuration_dir() .. "scripts/runner f 0")
	else
		panels:restore(c.screen)
		awful.spawn.with_shell(awful.util.get_configuration_dir() .. "scripts/runner f 1")
		if c.maximized or c.maximized_vertical and c.maximized_horizontal then
			c.border_width = 0
		else
			c.border_width = beautiful.border_width
		end
	end
end

function sighelpers.maximized_hook(c)
	if c.maximized or c.maximized_vertical and c.maximized_horizontal then
		c.border_width = 0
		c.screen.side.bg = beautiful.color.black .. "cc"
	else
		c.border_width = beautiful.border_width
		c.screen.side.bg = beautiful.color.none
	end
end

function sighelpers.cih(c)
	local _c = ""
	if c.sticky then
		_c = _c .. beautiful.tasklist_sticky
	end
	if c.ontop then
		_c = _c .. beautiful.tasklist_ontop
	elseif c.above then
		_c = _c .. beautiful.tasklist_above
	elseif c.below then
		_c = _c .. beautiful.tasklist_below
	end
	if c.maximized then
		_c = _c .. beautiful.tasklist_maximized
	else
		if c.maximized_horizontal then
			_c = _c .. beautiful.tasklist_maximized_horizontal
		end
		if c.maximized_vertical then
			_c = _c .. beautiful.tasklist_maximized_vertical
		end
		if c.floating then
			_c = _c .. beautiful.tasklist_floating
		end
	end
	if _c ~= "" then
		_c = " <span font='" .. beautiful.cprops_font .. "'><sup>" .. _c .. "</sup></span>"
	end
	return _c
end
-- }}}

-- {{{ SIGNALS
client.connect_signal("manage", function(c)
	-- if not awesome.startup then awful.client.setslave(c) end
	if awesome.startup and not c.size_hints.user_position then -- and not c.size_hints.program_position then
		awful.placement.no_offscreen(c)
		awful.placement.no_overlap(c)
		awful.placement.centered(c)
	end
end)
client.connect_signal("focus", function(c)
	c.border_color = beautiful.border_focus
	sighelpers.maximized_hook(c)
	sighelpers.fullscreen_hook(c)
	c.screen._ci.i:set_image(cicon(c))
	c.screen._ci.t:set_markup( (c.name or "Untitled") .. sighelpers.cih(c))
end)
client.connect_signal("unfocus", function(c)
	c.border_color = beautiful.border_normal
	sighelpers.maximized_hook(c)
	sighelpers.fullscreen_hook(c)
	c.screen._ci.i:set_image(beautiful.icon.empty) --beautiful.icon.prg.no_icon)
	c.screen._ci.t:set_text("") --"No client")
	-- c.screen.panel.bg = beautiful.color.black --.none
	c.screen.side.bg = beautiful.color.none
	panels:restore(c.screen)
end)
client.connect_signal("property::fullscreen", function(c)
	sighelpers.fullscreen_hook(c)
end)
client.connect_signal("property::maximized", function(c)
	sighelpers.maximized_hook(c)
	c.screen._ci.t:set_markup( (c.name or "Untitled") .. sighelpers.cih(c))
end)
client.connect_signal("property::icon", function(c)
	if c == client.focus then
		c.screen._ci.i:set_image(cicon(c))
	end
end)
client.connect_signal("property::name", function(c)
	if c == client.focus then
		c.screen._ci.t:set_markup( (c.name or "Untitled") .. sighelpers.cih(c))
	end
end)
-- {{{ TITLEBARS
client.connect_signal("request::titlebars", function(c)
	local buttons = awful.util.table.join(
		awful.button({}, 1, function()
			client.focus = c
			c:raise()
			awful.mouse.client.move(c)
		end),
		awful.button({}, 3, function()
			client.focus = c
			c:raise()
			awful.mouse.client.resize(c)
		end)
	)
	awful.titlebar(c):setup {
		{
			awful.titlebar.widget.iconwidget(c),
			buttons = buttons,
			layout = wibox.layout.fixed.horizontal
		},
		{
			{
				align = "center",
				widget = awful.titlebar.widget.titlewidget(c)
			},
			buttons = buttons,
			layout = wibox.layout.flex.horizontal
		},
		{
			awful.titlebar.widget.floatingbutton(c),
			awful.titlebar.widget.maximizedbutton(c),
			awful.titlebar.widget.stickybutton(c),
			awful.titlebar.widget.ontopbutton(c),
			awful.titlebar.widget.closebutton(c),
			layout = wibox.layout.fixed.horizontal()
		},
		layout = wibox.layout.align.horizontal
	}
end)
-- }}}
-- {{{ EXIT
-- awesome.connect_signal("exit", function()
--   awful.spawn.with_shell("/mnt/sdb1/bin/autostarter 'stop'")
-- end)
-- }}}
-- }}}
