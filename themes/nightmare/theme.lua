local getdir = require("awful.util").get_configuration_dir
local dir = getdir() .. "themes/nightmare/img/"

local theme = {
	color = {
		none = "#000000" .. "00",
		black = "#000000",
		white = "#FFFFFF",
		mark = "#FF5959",
		-- light = "#3d4047",
		normal = "#262a34",
		dark = "#2E3035",

		-- gtk = {
		--   mark = "#a62929"
		-- },

	},
	icon = {
		menu = dir .. "menu/main.png",
		sub = dir .. "menu/sub.png",
		empty = dir .. "empty.png",
		tag = {
			dev = dir .. "tags/dev.png",
			term = dir .. "tags/term.png",
			net = dir .. "tags/net.png",
			mail = dir .. "tags/mail.png",
			media = dir .. "tags/media.png",
			etc = dir .. "tags/etc.png"
		},
		tag_m = {
			selected = dir .. "tags/m/selected.png",
			selected_empty = dir .. "tags/m/selected_empty.png",
			clients = dir .. "tags/m/clients.png",
			empty = dir .. "tags/m/empty.png",
			urgent = dir .. "tags/m/urgent.png"
		},
		layout = {
			fair = dir .. "layouts/fair.png",
			tile_left = dir .. "layouts/tile_left.png",
			tile = dir .. "layouts/tile.png",
			max = dir .. "layouts/max.png",
			floating = dir .. "layouts/floating.png"
		},
		notify = {
			e = dir .. "notify/error.svg",
			i = dir .. "notify/info.svg",
			q = dir .. "notify/question.svg",
			w = dir .. "notify/warn.svg",
			calc = dir .. "notify/calc.svg",
			bat = dir .. "notify/battery.svg"
		},
		task = {
			-- normal = dir .. "tasks/normal.png",
			-- focus = dir .. "tasks/focus.png",
			urgent = dir .. "tasks/urgent.png",
			-- minimized = dir .. "tasks/minimized.png"
		},
		tray = {
			opened = dir .. "tray/opened.png",
			closed = dir .. "tray/closed.png"
		},
		prg = {
			no_icon = dir .. "prg/no_icon.svg",
			alsa = dir .. "prg/alsa.svg",
			fm = dir .. "prg/fm.svg",
			gimp = dir .. "prg/gimp.svg",
			gpg = dir .. "prg/gpg.svg",
			kbd = dir .. "prg/kbd.svg",
			mail = dir .. "prg/mail.svg",
			music = dir .. "prg/music.svg",
			nvim = dir .. "prg/nvim.svg",
			rss = dir .. "prg/rss.svg",
			sysmon = dir .. "prg/sysmon.svg",
			term = dir .. "prg/term.svg",
			web = dir .. "prg/web.svg",
			jabber = dir .. "prg/jabber.svg",
			x = dir .. "prg/x.svg",
		}
	},
	wallpaper = dir .. "bg.png",
	useless_gap = 2,
}

theme.font = "Open Sans 9"
theme.monofont = "Terminus 8"

theme.calendar = {
	padding = 12,
	font = "Terminus 8",
	bg = theme.color.black,
	fg = theme.color.white,
	rem_font = "tewi2a 8",
	rem_color = "#c000ff",
	show_desc = false
}

theme.timewidget = {
	font = "Terminus 10",
	color = theme.color.white
}

theme.kbd_font = "Terminus 8"
theme.kbd_fg = theme.color.white
theme.kbd_replacements = {
	["us"] = " eng ",
	["ru"] = " rus "
}

theme.ups = {
	bat = dir .. "ups/bat.png",
	nobat = dir .. "ups/nobat.png",
	chrg = dir .. "ups/chrg.png",
	err = dir .. "ups/err.png",
	warn = dir .. "ups/warn.png",
	none = dir .. "ups/none.png"
}

theme.mail = {
	new = dir .. "mail/new.png",
	no = dir .. "mail/no.png"
}

theme.led = dir .. "led.png"

theme.octavia = {
	onpanel = {
		["disconnect"] = dir .. "octavia/panel/disconnect.png",
		["play"] = dir .. "octavia/panel/play.png",
		["pause"] = dir .. "octavia/panel/pause.png",
		["previous"] = dir .. "octavia/panel/prev.png",
		["next"] = dir .. "octavia/panel/next.png",
		["show"] = dir .. "octavia/panel/show.png",
		font = "Open Sans 8"
	},
	widget = {
		["disconnect"] = dir .. "octavia/widget/disconnect.png",
		["play"] = dir .. "octavia/widget/play.png",
		["pause"] = dir .. "octavia/widget/pause.png",
		["stop"] = dir .. "octavia/widget/stop.png",
		["player"] = dir .. "octavia/widget/player.png",
		["previous"] = dir .. "octavia/widget/prev.png",
		["next"] = dir .. "octavia/widget/next.png",
		["repeat"] = dir .. "octavia/widget/repeat.png",
		["random"] = dir .. "octavia/widget/random.png",
		["single"] = dir .. "octavia/widget/single.png",
		["consume"] = dir .. "octavia/widget/consume.png",
		["volicon"] = dir .. "octavia/widget/volicon.png",
		["overlay"] = dir .. "octavia/widget/overlay.png",
		font = "Open Sans 8",
		big_font = "Open Sans Bold 9",
		small_font = "Open Sans 7"
	},
	["nocover"] = dir .. "octavia/nocover.png"
}

-- {{{ TAGLIST MARKERS
theme.taglist_squares_sel = theme.icon.tag_m.selected
theme.taglist_squares_sel_empty = theme.icon.tag_m.selected_empty
theme.taglist_squares_unsel = theme.icon.tag_m.clients
theme.taglist_squares_unsel_empty = theme.icon.tag_m.empty
theme.taglist_squares_urgent = theme.icon.tag_m.urgent
-- }}}

-- {{{ TAGLIST
--theme.taglist_fg_focus = theme.text
--theme.taglist_fg_occupied = white .. "40"
-- theme.taglist_fg_urgent = theme.urg
theme.taglist_bg_urgent = theme.color.none
theme.taglist_bg_focus = theme.color.none
-- }}}

-- {{{ CLIENT BORDERS
theme.border_width = 1
theme.border_normal = theme.color.black
theme.border_focus = theme.color.white
theme.border_marked = theme.color.mark
-- }}}

-- {{{ MENU
theme.menu_bg_normal = theme.color.black
theme.menu_fg_normal = theme.color.white
theme.menu_fg_focus = theme.color.white
theme.menu_bg_focus = theme.color.normal
theme.menu_border_width = 2
theme.menu_border_color = theme.color.black
theme.menu_submenu_icon = theme.icon.sub
theme.menu_height = 20
theme.menu_width  = 160
-- }}}

-- {{{ TOOLTIP
theme.tooltip_border_color = theme.color.black
theme.tooltip_bg = theme.color.black
theme.tooltip_fg = theme.color.white
theme.tooltip_font = theme.font
theme.tooltip_border_width = 0
theme.tooltip_opacity = 1
theme.tooltip_shape = nil
-- }}}

-- {{{ TASKLIST
theme.tasklist_bg_focus = theme.color.none
theme.tasklist_bg_urgent = theme.color.none
theme.tasklist_bg_normal = theme.color.none
theme.tasklist_bg_minimize = theme.color.none

-- theme.bg_image_normal = theme.icon.task.normal
-- theme.bg_image_focus = theme.icon.task.focus
theme.bg_image_urgent = theme.icon.task.urgent
-- theme.bg_image_minimize = theme.icon.task.minimized

theme.tasklist_disable_icon = false
theme.tasklist_plain_task_name = true

theme.tasklist_sticky = "s"
theme.tasklist_ontop = "t"
theme.tasklist_above = "a"
theme.tasklist_below = "b"
theme.tasklist_floating = "f"
theme.tasklist_maximized = "m"
theme.tasklist_maximized_horizontal = "h"
theme.tasklist_maximized_vertical = "v"

theme.cprops_font = "tewi2a 8"
-- }}}

-- {{{ LAYOUT
theme.layout_fairv = theme.icon.layout.fair
theme.layout_tileleft = theme.icon.layout.tile_left
theme.layout_tile = theme.icon.layout.tile
theme.layout_max = theme.icon.layout.max
theme.layout_floating = theme.icon.layout.floating
-- theme.layout_fairh = "/usr/share/awesome/themes/default/layouts/fairhw.png"
-- theme.layout_magnifier = "/usr/share/awesome/themes/default/layouts/magnifierw.png"
-- theme.layout_fullscreen = "/usr/share/awesome/themes/default/layouts/fullscreenw.png"
-- theme.layout_tilebottom = "/usr/share/awesome/themes/default/layouts/tilebottomw.png"
-- theme.layout_tiletop = "/usr/share/awesome/themes/default/layouts/tiletopw.png"
-- theme.layout_spiral  = "/usr/share/awesome/themes/default/layouts/spiralw.png"
-- theme.layout_dwindle = "/usr/share/awesome/themes/default/layouts/dwindlew.png"
-- theme.layout_cornernw = "/usr/share/awesome/themes/default/layouts/cornernww.png"
-- theme.layout_cornerne = "/usr/share/awesome/themes/default/layouts/cornernew.png"
-- theme.layout_cornersw = "/usr/share/awesome/themes/default/layouts/cornersww.png"
-- theme.layout_cornerse = "/usr/share/awesome/themes/default/layouts/cornersew.png"
-- }}}

-- -- -- -- -- -- -- -- -- --





theme.bg_normal = theme.color.black
theme.bg_focus      = "#535d6c"
theme.bg_urgent     = "#ff0000"
theme.bg_minimize   = "#444444"
theme.bg_systray = theme.bg_normal

theme.fg_normal     = "#aaaaaa"
theme.fg_focus      = "#ffffff"
theme.fg_urgent     = "#ffffff"
theme.fg_minimize   = "#ffffff"


-- {{{ SYSTEM TRAY
theme.bg_systray = theme.color.black
theme.systray_icon_spacing = 6
-- }}}

-- Display the taglist squares
-- theme.taglist_squares_sel   = "/usr/share/awesome/themes/default/taglist/squarefw.png"
-- theme.taglist_squares_unsel = "/usr/share/awesome/themes/default/taglist/squarew.png"

-- Define the image to load
theme.titlebar_close_button_normal = "/usr/share/awesome/themes/default/titlebar/close_normal.png"
theme.titlebar_close_button_focus  = "/usr/share/awesome/themes/default/titlebar/close_focus.png"

theme.titlebar_minimize_button_normal = "/usr/share/awesome/themes/default/titlebar/minimize_normal.png"
theme.titlebar_minimize_button_focus  = "/usr/share/awesome/themes/default/titlebar/minimize_focus.png"

theme.titlebar_ontop_button_normal_inactive = "/usr/share/awesome/themes/default/titlebar/ontop_normal_inactive.png"
theme.titlebar_ontop_button_focus_inactive  = "/usr/share/awesome/themes/default/titlebar/ontop_focus_inactive.png"
theme.titlebar_ontop_button_normal_active = "/usr/share/awesome/themes/default/titlebar/ontop_normal_active.png"
theme.titlebar_ontop_button_focus_active  = "/usr/share/awesome/themes/default/titlebar/ontop_focus_active.png"

theme.titlebar_sticky_button_normal_inactive = "/usr/share/awesome/themes/default/titlebar/sticky_normal_inactive.png"
theme.titlebar_sticky_button_focus_inactive  = "/usr/share/awesome/themes/default/titlebar/sticky_focus_inactive.png"
theme.titlebar_sticky_button_normal_active = "/usr/share/awesome/themes/default/titlebar/sticky_normal_active.png"
theme.titlebar_sticky_button_focus_active  = "/usr/share/awesome/themes/default/titlebar/sticky_focus_active.png"

theme.titlebar_floating_button_normal_inactive = "/usr/share/awesome/themes/default/titlebar/floating_normal_inactive.png"
theme.titlebar_floating_button_focus_inactive  = "/usr/share/awesome/themes/default/titlebar/floating_focus_inactive.png"
theme.titlebar_floating_button_normal_active = "/usr/share/awesome/themes/default/titlebar/floating_normal_active.png"
theme.titlebar_floating_button_focus_active  = "/usr/share/awesome/themes/default/titlebar/floating_focus_active.png"

theme.titlebar_maximized_button_normal_inactive = "/usr/share/awesome/themes/default/titlebar/maximized_normal_inactive.png"
theme.titlebar_maximized_button_focus_inactive  = "/usr/share/awesome/themes/default/titlebar/maximized_focus_inactive.png"
theme.titlebar_maximized_button_normal_active = "/usr/share/awesome/themes/default/titlebar/maximized_normal_active.png"
theme.titlebar_maximized_button_focus_active  = "/usr/share/awesome/themes/default/titlebar/maximized_focus_active.png"




theme.awesome_icon = "/usr/share/awesome/icons/awesome16.png"

-- Define the icon theme for application icons. If not set then the icons
-- from /usr/share/icons and /usr/share/icons/hicolor will be used.
theme.icon_theme = "Evolvere"

return theme
